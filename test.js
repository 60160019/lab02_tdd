const chai = require("chai");
const expect = chai.expect;
const validate = require("./module/validate");

describe("Validate Module", () => {
  context("isUserNameValid", () => {
    it("Function prototype : boolean isUserNameValid(username: String)", () => {
      expect(validate.isUserNameValid("aya")).to.be.true;
    });
    it("จำนวนตัวอักษรอย่างน้อย 3 ตัวอักษร", () => {
      expect(validate.isUserNameValid("ay")).to.be.false;
    });
    it("ทุกตัวต้องเป็นตัวเล็ก", () => {
      expect(validate.isUserNameValid("Kob")).to.be.false;
      expect(validate.isUserNameValid("koB")).to.be.false;
    });
    it("จำนวนตัวอักษรที่มากที่สุดคือ 15 ตัวอักษร", () => {
      expect(validate.isUserNameValid("kob123456789012")).to.be.true;
      expect(validate.isUserNameValid("kob1234567890123")).to.be.false;
    });
  });

  context("isAgeValid", () => {
    it("Function prototype : boolean isAgeValid (age: String)", () => {
      expect(validate.isAgeValid("18")).to.be.true;
    });
    it("age ต้องเป็นข้อความที่เป็นตัวเลข", () => {
      expect(validate.isAgeValid("a")).to.false;
    });
    it("อายุต้องไม่ต่ำกว่า 18 ปี และไม่เกิน 100 ปี", () => {
      expect(validate.isAgeValid(17)).to.be.false;
      expect(validate.isAgeValid(18)).to.be.true;
      expect(validate.isAgeValid(100)).to.be.true;
      expect(validate.isAgeValid(101)).to.be.false;
    });
  });
  context("isPasswordValid ", () => {
    it("Function prototype : boolean isUserNameValid(password: String)", () => {
      expect(validate.isPasswordValid("Aya@12345678")).to.be.true;
    });
    it("จำนวนตัวอักษรอย่างน้อย 8 ตัวอักษร", () => {
      expect(validate.isPasswordValid("A@12345")).to.be.false;
      expect(validate.isPasswordValid("Aya@123456789")).to.be.true;
    });
    it("ต้องมีอักษรตัวใหญ่เป็นส่วนประกอบอย่างน้อย 1 ตัว", () => {
      expect(validate.isPasswordValid("aya12345678")).to.be.false;
    });
    it("ต้องมีตัวเลขเป็นส่วนประกอบอย่างน้อย 3 ตัว", () => {
      expect(validate.isPasswordValid("Aya@zero123456")).to.be.true;
      expect(validate.isPasswordValid("Aya@z11bebelo")).to.be.false;
    });
    it("ต้องมีอักขระ พิเศษ !@#$%^&*()_+|~-=`{}[]:;'<>?,./ อย่างน้อย 1 ตัว", () => {
      expect(validate.isPasswordValid("ayabekza10")).to.be.false;
    });
  });
  context('isDateValid', () => {
    it('Function prototype : boolean isDateValid(day: Integer, month: Integer, year: Integer)', () => {
      expect(validate.isDateValid(1, 11, 2016)).to.be.true;
    });
    it('day เริ่ม 1 และไม่เกิน 31 ในทุก ๆ เดือน', () => {
      expect(validate.isDateValid(32, 12, 2016)).to.be.false;
    });
    it('month เริ่มจาก 1 และไม่เกิน 12 ในทุก ๆ เดือน', () => {
      expect(validate.isDateValid(32, 13, 2016)).to.be.false;
    });
    it('year จะต้องไม่ต่ำกว่า 1970 และ ไม่เกิน ปี 2020', () => {
      expect(validate.isDateValid(12, 5, 1969)).to.be.false;
      expect(validate.isDateValid(20, 2, 2021)).to.be.false;
    });
    it('ในกรณีของปี อธิกสุรทิต ต้องหาร 100 และ 400 ลงตัว', () => {
      expect(validate.isDateValid(29, 2, 2000)).to.be.true;
    });
  });
});
