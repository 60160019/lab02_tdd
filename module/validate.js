module.exports = {
  isUserNameValid: function (username) {
    if (username.length < 3 || username.length > 15) {
      return false;
    }
    if (username.toLowerCase() !== username) {
      return false;
    }
    return true;
  },
  isAgeValid: function (age) {
    if (isNaN(age)) {
      return false;
    }
    if (age < 18 || age > 100) {
      return false;
    }
    return true;
  },
  isPasswordValid: function (password) {
    const findUpperCase = /[A-Z]{1,}/;
    const findNumber = /[0-9]{3,}/;
    const findSpacialCharacter = /(?=.*[!@#\$%\^&\*]){1,}/;
    if (password.length < 8) {
      return false;
    }
    if (!findUpperCase.test(password)) {
      return false;
    }
    if (!password.match(findNumber)) {
      return false;
    }
    if (!password.match(findSpacialCharacter)) {
      return false;
    }
    return true;
  },
  isDateValid: function (day, month, year) {
    let leapYear = false;

    if ((day < 0) || (day > 31)) {
      return false;
    }
    if (month < 0 || month > 12) {
      return false;
    }
    if (year < 1970 || year > 2020) {
      return false;
    }
    if (year % 100 == 0 && year % 400 == 0) {
      leapYear = true
    } else {
      leapYear = false
    }
    switch (month) {
      case 1:
        if (day > 31) {
          return false;
        }
      case 2:
        if (leapYear) {
          if (day > 29) {
            return false
          }
        } else {
          if (day > 28) {
            return false
          }
        }
      case 3:
        if (day > 31) {
          return false
        }
      case 4:
        if (day > 30) {
          return false
        }
      case 5:
        if (day > 31) {
          return false
        }
      case 6:
        if (day > 30) {
          return false
        }
      case 7:
        if (day > 31) {
          return false
        }
      case 8:
        if (day > 31) {
          return false
        }
      case 9:
        if (day > 30) {
          return false
        }
      case 10:
        if (day > 31) {
          return false
        }
      case 11:
        if (day > 30) {
          return false
        }
      case 12:
        if (day > 31) {
          return false
        }
    }
    return true;
  }
};
